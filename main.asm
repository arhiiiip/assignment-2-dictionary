%include "lib.inc"
%include "colon.inc"
%include "words.inc"

%define MAX_SIZE 256
%define OFFSET 8

global _start

extern find_word

section .rodata
	start_work_message: db "Please enter the key", 0
	word_find: db "Your value", 0
	key_error: db "Key is not valid", 0
	found_fail: db "Key wasnt found.", 0
	symbol: db ":", 0

section .bss
	data_buffer: times MAX_SIZE db 0
		
section .text
_start:
	mov rdi, start_work_message
	call print_string
	call print_newline
	
	mov rsi, 256
	mov rdi, data_buffer
	call read_word
	test rax, rax
	jz .key_fail
	push rdx

	
	mov rsi, link
	mov rdi, data_buffer
	call find_word
	test rax, rax
	jz .found_fail
	
	push rax
	call print_newline
	pop rax
	add rax, OFFSET
	mov rdi, rax
	push rdi
	call print_string
	mov rdi, symbol
	call print_string
	
	pop rdi
	pop rdx
	
	add rdi, rdx
	inc rdi
	call print_string
	call print_newline
	call exit


.key_fail:
	mov rdi, overflow_message
	jmp .print_err
.found_fail:
	mov rdi, not_found_message
.print_err:
	call print_error
	call print_newline
	call exit	
