%define link 0

%macro colon 2
	%ifid %2
		%2: dq link
		
		%define link %2
		
	%else
		
		%error "Second argument is not pointer"
		
	%endif
	
	%ifstr %1
		
		db %1, 0
		
	%else
		
		%error "First argument is not string"
		
	%endif
%endmacro