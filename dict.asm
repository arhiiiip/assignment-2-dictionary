%include "lib.inc"

global find_word

;rdi - null-terminated string pointer
;rsi - dictionary start index


find_word:
	.loop:
		push rdi
		push rsi 
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi		
		test rax, rax
		jnz .word_found
		mov rsi, [rsi] 
		test rsi, rsi
		jz .word_not_found
		jmp .loop

	.word_found:
		mov rax, rsi
		jmp .end 

	.word_not_found: 
		mov rax, 0

	.end:
		ret
