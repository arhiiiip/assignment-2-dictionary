global exit;
global string_length;
global print_string;
global print_char;
global print_newline;
global print_uint;
global print_int;
global string_equals;
global read_char;
global read_word;
global parse_uint;
global parse_int;
global string_copy;
global print_error;

; Принимает код возврата и завершает текущий процесс
exit:     
	xor rax, 60
	syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину

string_length:
  	mov rax, 0
    	.loop:
    		xor rax, rax
    	.count:
      		cmp byte [rdi+rax], 0
      		je .end
      		inc rax
      		jmp .count
    	.end:
      		ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
    	mov  rdx, rax
    	mov  rsi, rdi
    	mov  rax, 1
    	mov  rdi, 1
    	syscall
    	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi		
	mov rdx, 1		
	mov rsi, rsp		
	mov rdi, 1		
	mov rax, 1		
	syscall
	pop rdi		
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA		
	call print_char		
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	push r11
	mov r11, rsp
		
	dec rsp
	mov byte [rsp], 0
	
	.loop:
		xor rdx, rdx
		div rdi
		add rdx, '0'
		dec rsp
		mov byte[rsp], dl
		test rax, rax
		jnz .loop
		
	mov rdi, rsp
	call print_string
	mov rsp, r11
	pop r11
	ret

	

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    	cmp rdi,  0
    	jae .out
    	push rdi
    	mov rdi, '-'
    	call print_char
    	pop rdi
    	neg rdi
    	.out:
    		call print_uint
	ret    	

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	call string_length
	mov rdx, rax	
	push rdi
	mov rdi, rsi
	call string_length
	mov rcx, rax	
	mov rsi, rdi
	pop rdi
	cmp rdx, rcx
	jne .notequals
	xor rax, rax
	.loop:				
		mov al, byte [rdi]
		cmp al, byte [rsi]
		jne .notequals
		inc rdi	
		inc rsi	
		cmp al, 0		
		jne .loop		
		
	.equals:				
		xor rax, rax
		inc rax
		ret
	.notequals:				
		xor rax, rax
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rdi, rdi		
	xor rdx, rdx		
	mov rdx, 1
	xor rax, rax
	push rax
	mov rsi, rsp
	syscall		
	pop rax
	ret

    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
	mov r14, rdi ;Start buffer
	mov r12, rsi ;Size buffer
	dec r12 ; null term
	xor r13, r13 ;counter

	.del_spaces:
		call read_char
		cmp rax, 0
		je .end
		cmp rax, 0x20
		je .del_spaces
		cmp rax, 0x9
		je .del_spaces
		cmp rax, 0xA
		je .del_spaces

	.loop:
		cmp r12, r13
		je .bad

		mov byte [r14+r13], al
		inc r13
		call read_char
		
		cmp rax, 0
		je .end
		cmp rax, 0x20
		je .end
		cmp rax, 0x9
		je .end
		cmp rax, 0xA
		je .end
		
		jmp .loop

	.bad:
		xor rax, rax
		pop r14
		pop r13
		pop r12
		ret

	.end:
		mov rax, r14
		mov rdx, r13
		mov byte[r14+r13], 0
		pop r14
		pop r13
		pop r12
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	xor r10, r10				
	xor r11, r11		
	mov r12, 10
	
	.loop:
		mov cl, byte[rdi + r10]	
		xor cl, "0"		
		cmp cl, 0x9		
		ja .end
		mul r8			
		add rax, r11		
		inc r10			
		jmp .loop
	.end:
		mov rdx, r10
		ret




; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov al, byte[rdi]
	cmp al, '-'
	jne .read
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret
	.read:
		call parse_uint
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
call string_length
	cmp rdx, rax
	jle .error
	xor rcx, rcx
	
	.loop:
		mov r10b, byte[rdi+rcx]
		mov byte[rsi+rcx], r10b
		inc rcx
		cmp r9b, 0
		je .end
		jmp .loop
		
	.end:
		ret	
	    
    	.error:
		xor rax, rax
		ret


print_error:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	syscall
	ret